#!/usr/bin/perl

require 5.005;
use strict;
local $^W=1;

use Test::More tests => 44;

require_ok('Configure::Parse');

my $cur_block = undef;      # name of current block in Configure::Parse::parse()

my $config = Configure::Parse->new();
isa_ok($config, 'Configure::Parse');
can_ok($config, 'parse');

ok($config->parse('New unit:'), 'new block');
$cur_block = $config->{'cur_block'};
is($cur_block, 'New unit', 'check name of current block');
ok(!$config->parse('bla-bla-blah'), 'simple string');
ok(!$config->parse('--confIg1 bla-bla-blah'), 'no white space at the start line');
ok($config->parse('  --config2 bla-bla-blah'), 'correct string');
ok($config->parse(' --config3'), 'correct line without description');

$config->parse(' --config4');
ok($config->parse('          help string'), 'description line');

ok($config->parse(''), 'end of block');

ok($config->parse('Installation directories:'), 'new block');
$cur_block = $config->{'cur_block'};
is($cur_block, 'Installation directories');

# parse strings
ok($config->parse('  --prefix=PREFIX         install architecture-independent files in PREFIX'));
is($config->{'order'}, 0);
ok($config->parse('                          [/usr/local/apache2]'));
is($config->{'order'}, 0);

ok($config->parse('  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX'));
is($config->{'order'}, 0);
ok($config->parse('                          [PREFIX]'));
is($config->{'order'}, 0);
ok(!$config->parse('By default, `make install\' will install all the files in'));
ok(!$config->parse('`/usr/local/apache2/bin\', `/usr/local/apache2/lib\' etc.  You can specify'));
ok(!$config->parse('an installation prefix other than `/usr/local/apache2\' using `--prefix\','));
ok(!$config->parse('for instance `--prefix=$HOME\'.'));

ok($config->parse(''), 'end of block');

# new block
ok($config->parse('Optional Features:'), 'new block');
$cur_block = $config->{'cur_block'};
is($cur_block, 'Optional Features', 'check name of current block');

ok($config->parse('  --enable-dtrace         Enable DTrace probes'),
                    'correct string containing name of config');
is($config->{'BLOCKS'}->{$cur_block}->{0}->{'enable-dtrace'}->{'help'}->{'count'}, 1);
cmp_ok($config->{'BLOCKS'}->{$cur_block}->{0}->{'enable-dtrace'}->{'help'}->{'message'}->[0],
    'eq', 'Enable DTrace probes');

ok($config->parse('  --enable-maintainer-mode'));
ok($config->parse('                          Turn on debugging and compile time warnings and load'));
ok($config->parse('                          all compiled modules'));
is($config->{'BLOCKS'}->{$cur_block}->{1}->{'enable-maintainer-mode'}->{'help'}->{'count'}, 2);
is($config->{'BLOCKS'}->{$cur_block}->{1}->{'enable-maintainer-mode'}->{'help'}->{'message'}->[0],
            'Turn on debugging and compile time warnings and load');
is($config->{'BLOCKS'}->{$cur_block}->{1}->{'enable-maintainer-mode'}->{'help'}->{'message'}->[1],
            'all compiled modules');

is($config->{'order'}, '2', 'chech number of order');
is($config->{'BLOCKS'}->{$cur_block}->{2}, undef);
ok($config->parse(''), 'end of block');
is($config->{'order'}, undef, '$self->{\'order\'} is undef');

ok($config->parse('Optional Packages:'), 'new block');
ok($config->parse('  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]'));
cmp_ok($config->{'order'}, 'eq', '0');