#!/usr/bin/perl

require 5.005;
use strict;
local $^W=1;

use Test::More tests => 26;

require_ok('Configure::Parse');

my $config = Configure::Parse->new();
can_ok($config, '_parse_line_config');

my $array = $config->_parse_line_config(' --with-config=123 help message');
is($array->[0], 'with-config', 'get config name');
is($array->[1], 'help message', 'get help message');

$array = undef;
$array = $config->_parse_line_config(' --config-name ');
is($array->[0], 'config-name', 'get config name');
is($array->[1], '', 'get blank help message');

$array = undef;
$array = $config->_parse_line_config(' --config-name');
is($array->[0], 'config-name', 'get config name');
is($array->[1], '', 'get blank help message');

$array = undef;
$array = $config->_parse_line_config('  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]');
is($array->[0], 'with-PACKAGE');
is($array->[1], 'use PACKAGE [ARG=yes]');

$array = undef;
$array = $config->_parse_line_config('  --disable-authn-file    file-based authentication control');
is($array->[0], 'disable-authn-file');
is($array->[1], 'file-based authentication control');
is($array->[2], 'bool');

$array = undef;
$array = $config->_parse_line_config('  --enable-authn-dbm      DBM-based authentication control');
is($array->[0], 'enable-authn-dbm');
is($array->[1], 'DBM-based authentication control');
is($array->[2], 'bool');

$array = undef;
$array = $config->_parse_line_config('  --with-suexec-bin       Path to suexec binary');
is($array->[0], 'with-suexec-bin');
is($array->[1], 'Path to suexec binary');
is($array->[2], 'bool');

$array = undef;
$array = $config->_parse_line_config('  --without-option    option 123');
is($array->[0], 'without-option');
is($array->[1], 'option 123');
is($array->[2], 'bool');

$array = undef;
$array = $config->_parse_line_config('--with-apr-util=PATH    prefix for installed APU or the full path to');
is($array->[0], 'with-apr-util');
is($array->[1], 'prefix for installed APU or the full path to');
is($array->[2], 'string');
is($array->[3], 'PATH');