package Configure::Parse;

require 5.005;
use strict;
local $^W=1; # use warnings only since 5.006

use IO::File;

our $VERSION = '0.5';

# list of ignored names configs
my %ignore_configs = (  'prefix'            => 1,
                        'exec-prefix'       => 1,
                        'with-PACKAGE'      => 1,
                        'without-PACKAGE'   => 1,
                        'disable-FEATURE'   => 1,
                        'enable-FEATURE'    => 1,
                        'build'             => 1,
                        'host'              => 1,
                        'target'            => 1,
                        'help'              => 1,
                        'version'           => 1,
                        'cache-file'        => 1,
                        'config-cache'      => 1,
                        'bindir'            => 1,
                        'sbindir'           => 1,
                        'libexecdir'        => 1,
                        'sysconfdir'        => 1,
                        'sharedstatedir'    => 1,
                        'localstatedir'     => 1,
                        'libdir'            => 1,
                        'includedir'        => 1,
                        'oldincludedir'     => 1,
                        'datarootdir'       => 1,
                        'datadir'           => 1,
                        'infodir'           => 1,
                        'localedir'         => 1,
                        'mandir'            => 1,
                        'docdir'            => 1,
                        'htmldir'           => 1,
                        'dvidir'            => 1,
                        'pdfdir'            => 1,
                        'psdir'             => 1 );

sub new {
    my $class = shift;
    bless {}, $class;
}

# method for parsing strings
sub parse {
    my ($self, $line) = @_;

    chomp($line);

    # new unit
    if ($line =~ qr/^(.+)\:$/) {
        $self->{'cur_block'} = $1;
        $self->{'cur_config'} = undef;
        $self->{'order'} = 0;
        return 1;
    }

    # end of unit
    if ($line =~ qr/^\s*$/) {
        $self->{'cur_block'} = undef;
        $self->{'cur_config'} = undef;
        $self->{'order'} = undef;
        return 1;
    }

    return unless (defined($self->{'cur_block'}));
    return if ($self->{'cur_block'} eq 'Configuration' ||
               $self->{'cur_block'} eq 'Some influential environment variables');

    my $block = $self->{'cur_block'};
    my $order = $self->{'order'};
    if ($line =~ qr/^\s{1,3}--.*$/) { # string of config
        my $parse_line_result = $self->_parse_line_config($line);
        my $config = $parse_line_result->[0];
        my $help_message = $parse_line_result->[1];
        my $config_type = $parse_line_result->[2];
        my $config_string_param = $parse_line_result->[3];
        unless ($config) {
            warn "can't get config name on line $line\n";
            return;
        }

        # check to see if the name of config on the list of ignored
        if (exists($ignore_configs{$config})) {
            return 1;
        }

        # add information to a variable
        $self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'help'}->{'count'} = 0;
        $self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'type'} = $config_type;
        if ($config_type eq 'string') {
            $self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'string_param'} = $config_string_param;
        }
        if ($help_message) {
            $self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'help'}->{'message'}->[0] = $help_message;
            $self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'help'}->{'count'}++;
        }
        $self->{'cur_config'} = $config;
        $self->{'order'}++;
        return 1;
    } elsif ($line =~ qr/^\s+.+$/) {   # not string of config
        if (defined($self->{'cur_config'})) {   # this string is help message
            my $config = $self->{'cur_config'};
            if ($order == 0) {
                return 1;
            }
            $order--;
            if (!exists($self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'help'}->{'count'})) {
                return 1;
            }
            my $help_count = $self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'help'}->{'count'};
            $line =~ s/^\s+(?=\S)//;    # delete spaces at beginning of line
            $self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'help'}->{'message'}->[$help_count] = $line;
            $self->{'BLOCKS'}->{$block}->{$order}->{$config}->{'help'}->{'count'}++;
            return 1;
        } else {
            return 1;
        }
    }

    return;
}

# sub-method for parsing string
sub _parse_line_config {
    my ($self, $line) = @_;
    $line =~ s/^\s+(?=\S)//;      # delete spaces at beginning of line
    my $config = $line;
    $config =~ s/^--(\S+)\s*(.*)$/$1/;    # get config name
    my $help = $2;
    $config =~ s/^(.+?)(?=(\[)).*$/$1/;
    my ($type, $string_param) = (undef, undef);
    if ($config =~ qr/^(.+)=(.+)$/) {
        $type = 'string';
        $config = $1;           # before '=' in old $config
        $string_param = $2;     # after '=' in old $config
    } else {
        $type = 'bool';
    }
    return [$config, $help, $type, $string_param];
}

# method for generate the output file
sub generate {
    my ($self, $title, $conf_in_file, $conf_list_file) = @_;

    my $conf_in_fh   = undef;     # file handler
    my $conf_list_fh = undef;     # file handler

    if ($conf_in_file) {
        eval {
            $conf_in_fh = IO::File->new("$conf_in_file", 'w') or die "$!";
        };
        if ($@) {
            warn "can't open file $conf_in_file: $@";
            return;
        }
    }
    elsif ($conf_in_file eq '-') {
        $conf_in_fh = *STDOUT;
    }
    else {
        $conf_in_fh = *STDOUT;
    }

    if ($conf_list_file) {
        eval {
            $conf_list_fh = IO::File->new("$conf_list_file", 'w') or die "$!";
        };
        if ($@) {
            warn "can't open file $conf_list_file: $@";
            return;
        }
    }

    if ($title) {
        print $conf_in_fh "menuconfig $title\n\n";
    }

    foreach my $block (keys %{$self->{'BLOCKS'}}) {
	foreach my $order (sort { $a <=> $b } keys %{$self->{'BLOCKS'}->{$block}}) {
	    foreach my $config (keys %{$self->{'BLOCKS'}->{$block}->{$order}}) {
                my $uc_config = uc($config);
                $uc_config =~ s/-/_/g;
                print $conf_in_fh "config $uc_config\n";
                print $conf_list_fh "CONFIG_".$uc_config."="."--".$config."\n"
                    if defined $conf_list_fh;

                # see on types of config
                my $config_pointer = $self->{'BLOCKS'}->{$block}->{$order}->{$config};
                if ($config_pointer->{'type'} eq 'bool') {
                    my $bool_string = $config;
                    print $conf_in_fh "\tbool \"$bool_string\"\n";
                    if ($config_pointer->{'help'}->{'count'} > 0) {
                        print $conf_in_fh "\t---help---\n";
                        foreach my $help_line (@{$config_pointer->{'help'}->{'message'}}) {
                            print $conf_in_fh "\t\t$help_line\n";
                        }
                    }
                } else {    # type is 'string'
                    my $str_string = $config . '=' . $config_pointer->{'string_param'};
                    print $conf_in_fh "\tstring \"$str_string\"\n".
                                "\tdefault \"\"\n";
                    if ($config_pointer->{'help'}->{'count'} > 0) {
                        print $conf_in_fh "\t---help---\n";
                        foreach my $help_line (@{$config_pointer->{'help'}->{'message'}}) {
                            print $conf_in_fh "\t\t$help_line\n";
                        }
                    }
                }
                print $conf_in_fh "\n\n";
            }
        }
    }

    undef $conf_list_fh;# closes the file
    undef $conf_in_fh;  # closes the file
}

1;

__END__

=head1 NAME

Configure::Parse

=head1 SYNOPSIS

  use Configure::Parse;

=head1 DESCRIPTION

=head1 AUTHOR

Alexander Ruzhnikov, <ruzhnikov85@gmail.com>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013, Dmitry Fedorov <dm.fedorov@gmail.com>
Written by Alexander Ruzhnikov <ruzhnikov85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut
